<!ENTITY torsettings.dialog.title "Axustes da rede Tor">
<!ENTITY torsettings.wizard.title.default "Conectar con Tor">
<!ENTITY torsettings.wizard.title.configure "Axustes da rede Tor">
<!ENTITY torsettings.wizard.title.connecting "Estabelecendo unha conexión">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Idioma do navegador Tor">
<!ENTITY torlauncher.localePicker.prompt "Seleccione un idioma.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Prema sobre «Conectar» para conectar con Tor.">
<!ENTITY torSettings.configurePrompt "Prema sobre «Configurar» para axustar a configuración da rede se está nun país que censure Tor (como Exipto, China, Turquía) ou se está a conectar desde unha rede privada que requira un servidor intermedio. ">
<!ENTITY torSettings.configure "Configurar">
<!ENTITY torSettings.connect "Conectar">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Á espera de que Tor comece ...">
<!ENTITY torsettings.restartTor "Reiniciar Tor">
<!ENTITY torsettings.reconfigTor "Reconfigurar">

<!ENTITY torsettings.discardSettings.prompt "Ten configuradas pontes Tor ou introduciu os axustes dun servidor intermedio local. &#160; Para facer unha conexión directa coa rede Tor, esta configuración debe ser retirada.">
<!ENTITY torsettings.discardSettings.proceed "Retirar a configuración e conectar">

<!ENTITY torsettings.optional "Opcional">

<!ENTITY torsettings.useProxy.checkbox "Emprego un proxy para conectarme a Internet.">
<!ENTITY torsettings.useProxy.type "Tipo de Proxy">
<!ENTITY torsettings.useProxy.type.placeholder "seleccione un tipo de servidor intermedio">
<!ENTITY torsettings.useProxy.address "Enderezo">
<!ENTITY torsettings.useProxy.address.placeholder "Enderezo IP ou nome da máquina">
<!ENTITY torsettings.useProxy.port "Porto">
<!ENTITY torsettings.useProxy.username "Nome de usuario">
<!ENTITY torsettings.useProxy.password "Contrasinal">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Este computador pasa por un firewall que só permite conexións a certos portos">
<!ENTITY torsettings.firewall.allowedPorts "Portos permitidos">
<!ENTITY torsettings.useBridges.checkbox "Tor está censurado no meu país">
<!ENTITY torsettings.useBridges.default "Seleccione unha ponte incorporada">
<!ENTITY torsettings.useBridges.default.placeholder "Seleccione unha ponte">
<!ENTITY torsettings.useBridges.bridgeDB "Solicite unha ponte de torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Escriba os caracteres da imaxe">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Obter un novo reto">
<!ENTITY torsettings.useBridges.captchaSubmit "Enviar">
<!ENTITY torsettings.useBridges.custom "Fornecer unha ponte coñecida">
<!ENTITY torsettings.useBridges.label "Engadir información dunha ponte dende unha fonte fiábel">
<!ENTITY torsettings.useBridges.placeholder "escribir enderezo:porto (un por liña)">

<!ENTITY torsettings.copyLog "Copia o rexistro do Tor ao portapapeis">

<!ENTITY torsettings.proxyHelpTitle "Axuda do proxy">
<!ENTITY torsettings.proxyHelp1 "Un proxy local pode ser necesario cando se conecte a través da rede dunha empresa, escola ou universidade.&#160;Se non está seguro cando un servidor intermedio é necesario, mire a configuración da Internet noutro navegador ou comprobe a configuración de rede do seu sistema.">

<!ENTITY torsettings.bridgeHelpTitle "Axuda do Repetidor Ponte">
<!ENTITY torsettings.bridgeHelp1 "As pontes son reenviadores que fan máis difícil bloquear as conexións á rede Tor.&#160;Cada tipo de ponte utiliza un método diferente para evitar a censura.&#160;As obfs por exemplo fan que o seu tráfico pareza un ruído aleatorio, e os meek fan o que o seu tráfico pareza que se estea conectando a ese servizo en lugar de a Tor.">
<!ENTITY torsettings.bridgeHelp2 "Debido a que certos países tentar bloquear Tor, algunhas pontes funcionan nalgúns países mais non noutros.&#160;Se non está seguro de que pontes funcionan no seu país, visite torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Agarde mentres se estabelece unha conexión coa rede Tor.&#160; Pode levar algúns minutos.">

<!-- #31286 about:preferences strings  -->
<!ENTITY torPreferences.categoryTitle "Tor">
<!ENTITY torPreferences.torSettings "Configuración do Tor">
<!ENTITY torPreferences.torSettingsDescription "O navegador Tor enruta o seu tráfico sobre a rede Tor, xestionada por milleiros de voluntarios de todo o mundo." >
<!ENTITY torPreferences.learnMore "Saber máis">
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescription "Quickstart allows Tor Browser to connect automatically.">
<!ENTITY torPreferences.quickstartCheckbox "Always connect automatically">
<!ENTITY torPreferences.bridges "Pontes">
<!ENTITY torPreferences.bridgesDescription "As pontes axudan a acceder á rede Tor en lugares onde Tor está bloqueado. Dependendo de onde estea vostede, unha ponte pode funcionar mellor ca outra.">
<!ENTITY torPreferences.useBridge "Utilizar unha ponte">
<!ENTITY torPreferences.requestNewBridge "Solicitar unha nova ponte...">
<!ENTITY torPreferences.provideBridge "Fornecer unha ponte">
<!ENTITY torPreferences.advanced "Avanzados">
<!ENTITY torPreferences.advancedDescription "Configurar como o navegador Tor se conecta á Internet.">
<!ENTITY torPreferences.firewallPortsPlaceholder "Valores separados por comas">
<!ENTITY torPreferences.requestBridgeDialogTitle "Pedir unha ponte">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Contactando con BridgeDB. Agarde.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Resolva o CAPTCHA para pedir unha ponte.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "A solución non é correcta. Ténteo de novo outra vez.">
<!ENTITY torPreferences.viewTorLogs "Ver os rexistros do Tor">
<!ENTITY torPreferences.viewLogs "Ver rexistros...">
<!ENTITY torPreferences.torLogsDialogTitle "Rexistros do Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.tryAgain "Try Connecting Again">
<!ENTITY torConnect.offline "Desconectado">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect to the Tor Network">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.connectingConcise "Conectando...">
<!ENTITY torConnect.connectedConcise "Conectado">
<!ENTITY torConnect.copyLog "Copy Tor Logs">
