<!ENTITY torsettings.dialog.title "Tor ցանցի կարգավորումներ">
<!ENTITY torsettings.wizard.title.default "Միանալ Tor-ին">
<!ENTITY torsettings.wizard.title.configure "Tor ցանցի կարգավորումներ">
<!ENTITY torsettings.wizard.title.connecting "Հիմնվում է միացում">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor դիտարկիչի լեզուն">
<!ENTITY torlauncher.localePicker.prompt "Խնդրում ենք ընտրել լեզուն։">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Սեղմեք «Միացնել» Tor-ին միանալու համար։">
<!ENTITY torSettings.configurePrompt "Սեղմեք «Կարգավորել» ցանցի կարգավորումները հարմարեցնելու համար՝ եթե դուք գտնվում եք Tor-ը գրաքննող երկրում (ինչպիսիք են՝ Եգիպտոսը, Չինաստանը, Թուրքիան) կամ միանում եք միջնացանց պահանջող մասնավոր ցանցից:">
<!ENTITY torSettings.configure "Կարգավորել">
<!ENTITY torSettings.connect "Միացնել">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Սպասվում է Tor-ի սկիզբը…">
<!ENTITY torsettings.restartTor "Վերսկսել Tor-ը">
<!ENTITY torsettings.reconfigTor "Վերակարգավորել">

<!ENTITY torsettings.discardSettings.prompt "Դուք կարգավորել եք Tor կամուրջները կամ տեղական միջնացանը:&#160; Tor ցանցին անմիջական միանալու համար, այդ կարգավորումները պետք է հեռացվեն:">
<!ENTITY torsettings.discardSettings.proceed "Հեռացնել կարգավորումները և միանալ">

<!ENTITY torsettings.optional "Կամընտրական">

<!ENTITY torsettings.useProxy.checkbox "Համացանցին միանալու համար ես օգտվում եմ միջնացանցից">
<!ENTITY torsettings.useProxy.type "Միջնացանցի տեսակ">
<!ENTITY torsettings.useProxy.type.placeholder "ընտրեք միջնացանցի տեսակը">
<!ENTITY torsettings.useProxy.address "Հասցե">
<!ENTITY torsettings.useProxy.address.placeholder "IP հասցե կամ հյուրընկալի անուն">
<!ENTITY torsettings.useProxy.port "Մատույց">
<!ENTITY torsettings.useProxy.username "Օգտագործողի անուն">
<!ENTITY torsettings.useProxy.password "Գաղտնաբառ">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Այս համակարգիչը անցնում է միայն որոշ մատույցներ թույլատրող հրապատի միջով">
<!ENTITY torsettings.firewall.allowedPorts "Թույլտատրված մատույցներ">
<!ENTITY torsettings.useBridges.checkbox "Tor-ը գրաքննվում է իմ երկրում">
<!ENTITY torsettings.useBridges.default "Ընտրեք ներկառուցված կամուրջ">
<!ENTITY torsettings.useBridges.default.placeholder "ընտրել կամուրջը">
<!ENTITY torsettings.useBridges.bridgeDB "Պահանջել կամուրջ torproject.org-ից">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Մուտքագրեք նկարում պատկերված խորհրդանիշները">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Get a new challenge">
<!ENTITY torsettings.useBridges.captchaSubmit "Ուղարկել">
<!ENTITY torsettings.useBridges.custom "Տրամադրել իմ իմացած կամուրջները">
<!ENTITY torsettings.useBridges.label "Մուտքագրել թույլատված աղբյուրից կամուջի տեղեկությունը">
<!ENTITY torsettings.useBridges.placeholder "type address:port (one per line)">

<!ENTITY torsettings.copyLog "Պատճենել Tor-ի մատյանը սեղանակատախտակի վրա">

<!ENTITY torsettings.proxyHelpTitle "Միջնացանցի օգնություն">
<!ENTITY torsettings.proxyHelp1 "A local proxy might be needed when connecting through a company, school, or university network.&#160;If you are not sure whether a proxy is needed, look at the Internet settings in another browser or check your system's network settings.">

<!ENTITY torsettings.bridgeHelpTitle "Bridge Relay Help">
<!ENTITY torsettings.bridgeHelp1 "Bridges are unlisted relays that make it more difficult to block connections to the Tor Network.&#160; Each type of bridge uses a different method to avoid censorship.&#160; The obfs ones make your traffic look like random noise, and the meek ones make your traffic look like it's connecting to that service instead of Tor.">
<!ENTITY torsettings.bridgeHelp2 "Because of how certain countries try to block Tor, certain bridges work in certain countries but not others.&#160; If you are unsure about which bridges work in your country, visit torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Խնդրում ենք սպասել մինչև Tor ցանցի հետ միացման հիմնումը:&#160; Այն կարող է տևել մի քանի րոպե:">

<!-- #31286 about:preferences strings  -->
<!ENTITY torPreferences.categoryTitle "Tor">
<!ENTITY torPreferences.torSettings "Tor կարգավորումներ">
<!ENTITY torPreferences.torSettingsDescription "Tor դիտարկիչը ուղղորդում է Ձեր երթևեկը Tor ցանցի միջոցով, որը գործարկում են հազարավոր կամավորներ ամբողջ աշխարհում: " >
<!ENTITY torPreferences.learnMore "Իմանալ ավելին">
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescription "Quickstart allows Tor Browser to connect automatically.">
<!ENTITY torPreferences.quickstartCheckbox "Always connect automatically">
<!ENTITY torPreferences.bridges "Կամուրջներ">
<!ENTITY torPreferences.bridgesDescription "Կամուրջներն օգնում են Ձեզ Tor-ը արգելափակված տեղերում Tor ցանցի մատչելիություն ունենսլ: Կախված նրանից թե որտեղ եք, մի կամուրջ կարող է ավելի լավ աշխատել քան մյուսը:">
<!ENTITY torPreferences.useBridge "Օգտագործել կամուրջը">
<!ENTITY torPreferences.requestNewBridge "Պահանջել նոր կամուրջ…">
<!ENTITY torPreferences.provideBridge "Տրամադրել կամուրջ">
<!ENTITY torPreferences.advanced "Advanced">
<!ENTITY torPreferences.advancedDescription "Կարգավորել թե ինչպես Tor դիտարկիչը միանա համացանցին:">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-separated values">
<!ENTITY torPreferences.requestBridgeDialogTitle "Պահանջել կամուրջ">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Միացում BridgeDB-ին։ Խնդրում ենք սպասել։">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Լուծեք «CAPTCHA»-ն կամուրջ պահանջելու համար:">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Լուծումը ճիշտ չէ: Խնդրում ենք փորձել կրկին:">
<!ENTITY torPreferences.viewTorLogs "Տեսնել Tor-ի մատյանները:">
<!ENTITY torPreferences.viewLogs "Տեսնել մատյանները...">
<!ENTITY torPreferences.torLogsDialogTitle "Tor-ի մատյաններ">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.tryAgain "Try Connecting Again">
<!ENTITY torConnect.offline "Անցանց">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect to the Tor Network">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.connectingConcise "Connecting…">
<!ENTITY torConnect.connectedConcise "Միացվեց">
<!ENTITY torConnect.copyLog "Copy Tor Logs">
